﻿using System.Collections.Generic;

namespace EventQueuePatternExample
{
    /// <summary>
    /// Event Manager
    /// </summary>
    public class EventQueueManager
    {
        private static EventQueueManager instance;

        public static EventQueueManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventQueueManager();
                }

                return instance;
            }
        }

        //Generic agent
        public delegate void EventDelegateX<T>(T e) where T : GameEvent;

        //Ordinary agent
        private delegate void EventDelegateX(GameEvent e);

        private Dictionary<System.Type, EventDelegateX> DelegatesMap = new Dictionary<System.Type, EventDelegateX>();
        private Dictionary<System.Delegate, EventDelegateX> DelegateLookupMap = new Dictionary<System.Delegate, EventDelegateX>();

        /// <summary>
        /// Add Listener
        /// </summary>
        public void AddListener<T>(EventDelegateX<T> del) where T : GameEvent
        {
            EventDelegateX internalDelegate = (e) => { del((T) e); };

            //Already exists, return
            if (DelegateLookupMap.ContainsKey(del) && DelegateLookupMap[del] == internalDelegate)
            {
                return;
            }

            //Join the delegateLookup
            DelegateLookupMap[del] = internalDelegate;

            //Join delegates
            EventDelegateX tempDel;
            if (DelegatesMap.TryGetValue(typeof(T), out tempDel))
            {
                DelegatesMap[typeof(T)] = tempDel += internalDelegate;
            }
            else
            {
                DelegatesMap[typeof(T)] = internalDelegate;
            }
        }

        /// <summary>
        /// Delete Listener
        /// </summary>
        public void RemoveListener<T>(EventDelegateX<T> del) where T : GameEvent
        {
            EventDelegateX internalDelegate;
            if (DelegateLookupMap.TryGetValue(del, out internalDelegate))
            {
                EventDelegateX tempDel;
                if (DelegatesMap.TryGetValue(typeof(T), out tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        DelegatesMap.Remove(typeof(T));
                    }
                    else
                    {
                        DelegatesMap[typeof(T)] = tempDel;
                    }
                }

                DelegateLookupMap.Remove(del);
            }
        }

        /// <summary>
        /// Add an event to the queue
        /// </summary>
        public void AddEventToQueue(GameEvent e)
        {
            EventDelegateX del;
            if (DelegatesMap.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);
            }
        }
    }
}