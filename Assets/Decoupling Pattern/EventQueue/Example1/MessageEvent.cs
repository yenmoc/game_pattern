﻿using System;

namespace EventQueuePatternExample
{
    /// <summary>
    /// Event entity class
    /// </summary>
    public class MessageEvent : GameEvent, IMessageEvent
    {
        public DateTime TimeRaised { get; }
        public MessagePriority Priority { get; }
        public float DisplayTime { get; }
        public object Message { get; }

        public MessageEvent(object message, float displayTime, MessagePriority priority)
        {
            Message = message;
            DisplayTime = displayTime;
            Priority = priority;
            TimeRaised = DateTime.Now;
        }
    }
}