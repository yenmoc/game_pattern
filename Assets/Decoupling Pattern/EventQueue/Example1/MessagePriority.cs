﻿namespace EventQueuePatternExample
{
    /// <summary>
    /// Event priority enumeration
    /// </summary>
    public enum MessagePriority
    {
        Low,
        Medium,
        High
    }
}