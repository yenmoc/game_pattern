﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
public class ServiceLocator
{
    private static readonly IDictionary<Type, object> ServiceCache;
    private static ServiceLocator instance;

    static ServiceLocator()
    {
        ServiceCache = new Dictionary<Type, object>();
    }

    // ReSharper disable once ConvertToNullCoalescingCompoundAssignment
    public static ServiceLocator Instance => instance ?? (instance = new ServiceLocator());

    public void Register<T>(T service)
    {
        var key = typeof(T);
        if (!ServiceCache.ContainsKey(key))
        {
            ServiceCache.Add(key, service);
        }
        else // overwrite the existing instance.
        {
            ServiceCache[key] = service;
        }
    }

    public T Resolve<T>()
    {
        var key = typeof(T);
        if (!ServiceCache.ContainsKey(key))
        {
            throw new ArgumentException($"Type '{key.Name}' has not been registered.");
        }

        return (T) ServiceCache[key];
    }

    public void Clear()
    {
        ServiceCache.Clear();
    }
}