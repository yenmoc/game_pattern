﻿//-------------------------------------------------------------------------------------
//	SingletonPatternExample2.cs
//-------------------------------------------------------------------------------------

using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Singleton
{
    public class SingletonPatternExample2 : MonoBehaviour
    {
        private void Start()
        {
            //RenderManager.Instance.TODO
        }
    }

    
    public class RenderManager
    {
        private static RenderManager instance;
        public static RenderManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RenderManager();
                }
                return instance;
            }
        }
    }
}