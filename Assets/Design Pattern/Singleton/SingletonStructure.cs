﻿//-------------------------------------------------------------------------------------
//	SingletonStructure.cs
//-------------------------------------------------------------------------------------

using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Singleton
{
    public class SingletonStructure : MonoBehaviour
    {
        void Start()
        {
            // Constructor is protected -- cannot use new
            Singleton s1 = Singleton.Instance();
            Singleton s2 = Singleton.Instance();

            // Test for same instance
            if (s1 == s2)
            {
                Debug.Log("Objects are the same instance");
            }
        }
    }

    /// <summary>
    /// The 'Singleton' class
    /// </summary>
    class Singleton
    {
        private static Singleton instance;

        // Constructor is 'protected'
        protected Singleton()
        {
        }

        public static Singleton Instance()
        {
            // Uses lazy initialization.
            // Note: this is not thread safe.
            if (instance == null)
            {
                instance = new Singleton();
            }

            return instance;
        }
    }
}