﻿//-------------------------------------------------------------------------------------
//	SingletonPatternExample3.cs
//-------------------------------------------------------------------------------------

using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Singleton
{
    public class SingletonPatternExample3 : MonoBehaviour
    {
        private void Start()
        {
            SingletonA.Instance.DoSomething();
            SingletonB.Instance.DoSomething();
        }
    }

    public abstract class Singleton<T> where T : class, new()
    {
        private static T instance = null;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                    return instance;
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            instance = this as T;
        }
    }


    public class SingletonA : Singleton<SingletonA>
    {
        public void DoSomething()
        {
            Debug.Log("SingletonA:DoSomething!");
        }
    }

    public class SingletonB : Singleton<SingletonB>
    {
        public void DoSomething()
        {
            Debug.Log("SingletonB:DoSomething!");
        }
    }
}