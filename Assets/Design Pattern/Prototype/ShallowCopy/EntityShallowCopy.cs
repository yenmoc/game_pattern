﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable once CheckNamespace
namespace DesignPattern.Prototype
{
    public abstract class EnemyPrototype
    {
        public abstract EnemyPrototype Clone();
    }

    public class EntityShallowCopy : EnemyPrototype
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        public EntityShallowCopy(int id, string name, int damage)
        {
            Id = id;
            Name = name;
            Damage = damage;
        }

        public override EnemyPrototype Clone()
        {
            return this.MemberwiseClone() as EnemyPrototype;
        }
    }
}