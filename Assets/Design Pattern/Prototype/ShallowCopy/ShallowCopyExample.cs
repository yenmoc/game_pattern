﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Prototype
{
    public class ShallowCopyExample : MonoBehaviour
    {
        private void Start()
        {
            EntityShallowCopy shallow1 = new EntityShallowCopy(1, "shallow entity 1", 20);
            EntityShallowCopy shallow2 = shallow1.Clone() as EntityShallowCopy;
            shallow2.Id = 2;
            
            Debug.Log(shallow1.Id + "==" + shallow1.Name + "==" + shallow1.Damage);
            Debug.Log(shallow2.Id + "==" + shallow2.Name + "==" + shallow2.Damage);
        }
    }
}