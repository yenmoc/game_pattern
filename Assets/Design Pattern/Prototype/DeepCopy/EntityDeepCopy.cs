﻿using System;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable once CheckNamespace
namespace DesignPattern.Prototype
{
    public class EntityDeepCopy : BaseCharacter, ICloneable
    {
        public EntityDeepCopy(int id, string name, int damage) : base(id, name, damage)
        {
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class BaseCharacter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        protected BaseCharacter(int id, string name, int damage)
        {
            Id = id;
            Name = name;
            Damage = damage;
        }
    }
}