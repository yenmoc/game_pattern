﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Prototype
{
    public class DeepCopyExample : MonoBehaviour
    {
        private void Start()
        {
            EntityDeepCopy entity1 = new EntityDeepCopy(1, "entity1", 10);
            EntityDeepCopy entity2 = (EntityDeepCopy) entity1.Clone();
            entity2.Id = 2;

            Debug.Log(entity1.Id + "==" + entity1.Name + "==" + entity1.Damage);
            Debug.Log(entity2.Id + "==" + entity2.Name + "==" + entity2.Damage);
            
            
            CloneFactory factory = new CloneFactory();
            EntityDeepCopyWithFactory entity3 = new EntityDeepCopyWithFactory("entity f1");
            EntityDeepCopyWithFactory entity4 = factory.GetClone(entity3) as EntityDeepCopyWithFactory;
            entity4.Name = "entity f2";
            
            Debug.Log(entity3.Name);
            Debug.Log(entity4.Name);
        }
    }
}