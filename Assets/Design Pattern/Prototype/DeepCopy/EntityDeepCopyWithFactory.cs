﻿using System;

// ReSharper disable once CheckNamespace
namespace DesignPattern.Prototype
{
    public class EntityDeepCopyWithFactory : ICharacter
    {
        public string Name { get; set; }

        public EntityDeepCopyWithFactory(string name)
        {
            Name = name;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public interface ICharacter : ICloneable
    {
        object Clone();
    }

    public class CloneFactory
    {
        public ICharacter GetClone(ICharacter character)
        {
            return character.Clone() as ICharacter;
        }
    }
}