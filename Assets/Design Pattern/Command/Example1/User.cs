﻿using System.Collections.Generic;
using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample1
{
    // The 'Invoker' class
    public class User
    {
        private Calculator _calculator = new Calculator();
        private List<Command> _commands = new List<Command>();
        private int _current;

        public void Redo(int level)
        {
            for (int i = 0; i < level; i++)
            {
                if (_current < _commands.Count - 1)
                {
                    Command command = _commands[_current++];
                    command.Execute();
                }
            }
        }

        public void Undo(int level)
        {
            UnityEngine.Debug.Log("\n---- Undo " + level + " levels");
            for (int i = 0; i < level; i++)
            {
                if (_current > 0)
                {
                    Command command = _commands[--_current];
                    command.UnExecute();
                }
            }
        }

        public void Compute(char @operator, int value)
        {
            Command command = new CalculatorCommand(_calculator, @operator, value);
            command.Execute();
            _commands.Add(command);
            _current++;
        }
    }
}