﻿using System;
using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample1
{
    // The 'ConcreteCommand' class
    public class CalculatorCommand : Command
    {
        private char _operator;
        private int _value;
        private Calculator _calculator;

        public CalculatorCommand(Calculator calculator, char @operator, int value)
        {
            this._calculator = calculator;
            this._operator = @operator;
            this._value = value;
        }


        public override void Execute()
        {
            _calculator.Operation(_operator, _value);
        }

        public override void UnExecute()
        {
            _calculator.Operation(Undo(_operator), _value);
        }

        private static char Undo(char @operator)
        {
            switch (@operator)
            {
                case '+': return '-';
                case '-': return '+';
                case '*': return '/';
                case '/': return '*';
                default:
                    throw new ArgumentException("@operator");
            }
        }
    }
}