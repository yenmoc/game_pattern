﻿// ReSharper disable once CheckNamespace
namespace CommandExample1
{
    // The 'Receiver' class
    public class Calculator
    {
        private int _value;

        public void Operation(char @operator, int value)
        {
            switch (@operator)
            {
                case '+':
                    _value += value;
                    break;
                case '-':
                    _value -= value;
                    break;
                case '*':
                    _value *= value;
                    break;
                case '/':
                    _value /= value;
                    break;
                default:
                    break;
            }

            UnityEngine.Debug.Log("Current value = " + _value + " ( following " + @operator + value + " )");
        }
    }
}