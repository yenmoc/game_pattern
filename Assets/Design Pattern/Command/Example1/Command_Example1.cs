﻿using CommandExample1;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class Command_Example1 : MonoBehaviour
{
    void Start()
    {
        // Create user and let her compute
        User user = new User();

        // User presses calculator buttons
        user.Compute('+', 100);
        user.Compute('-', 50);
        user.Compute('*', 10);
        user.Compute('/', 2);

        // Undo 4 commands
        user.Undo(4);

        // Redo 3 commands
        user.Redo(3);
    }
}