﻿using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class TurnTVOn : ICommand
    {
        IElectronicDevice device;

        public TurnTVOn(IElectronicDevice device)
        {
            this.device = device;
        }

        public void Execute()
        {
            this.device.On();
        }

        public void Undo()
        {
            this.device.Off();
        }
    }
}