﻿using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class TurnTVOff : ICommand
    {
        IElectronicDevice device;

        public TurnTVOff(IElectronicDevice device)
        {
            this.device = device;
        }

        public void Execute()
        {
            this.device.Off();
        }

        public void Undo()
        {
            this.device.On();
        }
    }
}