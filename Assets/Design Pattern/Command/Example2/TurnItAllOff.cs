﻿using System.Collections.Generic;
using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class TurnItAllOff : ICommand
    {
        List<IElectronicDevice> devices;

        public TurnItAllOff(List<IElectronicDevice> devices)
        {
            this.devices = devices;
        }

        public void Execute()
        {
            foreach (IElectronicDevice device in devices)
            {
                device.Off();
            }
        }

        public void Undo()
        {
            foreach (IElectronicDevice device in devices)
            {
                device.On();
            }
        }
    }
}