﻿using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class TurnVolumeUp : ICommand
    {
        IElectronicDevice device;

        public TurnVolumeUp(IElectronicDevice device)
        {
            this.device = device;
        }

        public void Execute()
        {
            this.device.VolumeUp();
        }

        public void Undo()
        {
            this.device.VolumeDown();
        }
    }
}