﻿using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class TurnVolumeDown : ICommand
    {
        IElectronicDevice device;

        public TurnVolumeDown(IElectronicDevice device)
        {
            this.device = device;
        }

        public void Execute()
        {
            this.device.VolumeDown();
        }

        public void Undo()
        {
            this.device.VolumeUp();
        }
    }
}