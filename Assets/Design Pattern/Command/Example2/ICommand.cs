﻿// ReSharper disable once CheckNamespace
namespace DesignPattern.Command
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}