﻿// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public interface IElectronicDevice
    {
        void On();
        void Off();
        void VolumeUp();
        void VolumeDown();
    }
}


