﻿using DesignPattern.Command;

// ReSharper disable once CheckNamespace
namespace CommandExample2
{
    public class DeviceButton
    {
        private ICommand _cmd;

        public DeviceButton(ICommand cmd)
        {
            this._cmd = cmd;
        }

        public void Press()
        {
            this._cmd.Execute(); // actually the invoker (device button) has no idea what it does
        }

        public void PressUndo()
        {
            this._cmd.Undo();
        }
    }

}