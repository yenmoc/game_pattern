# game_pattern

## What

* 

## Requirement

* 

## Install

```shell
yarn add "git+ssh://git@gitlab.com:yenmoc/game_pattern"
```

## Usage

* ```csharp

*Prototype Pattern
The classes and objects participating in this pattern are:
-	Prototype (BaseCharacter)  : declares an interface for cloning itself
-	ConcretePrototype (EntityDeepCopy) : implements an operation for cloning itself
-	Client (DeepCopyExample) : creates a new object by asking a prototype to clone itself

##Lợi ích của Prototype Pattern là gì?
Cãi thiện performance: giảm chi phí để tạo ra một đối tượng mới theo chuẩn, điều này sẽ làm tăng hiệu suất so với việc sử dụng từ khóa new để tạo đối tượng mới.
Giảm độ phức tạp cho việc khởi tạo đối tượng: do mỗi lớp chỉ implement cách clone của chính nó.
Giảm việc phân lớp, tránh việc tạo nhiều lớp con cho việc khởi tạo đối tượng như của Abstract Factory Pattern.
Khởi tạo object mới bằng cách thay đổi một vài thuộc tính của object (các object có ít điểm khác biệt nhau): Một hệ thống linh động sẽ để cho chúng ta tự định nghĩa một hành động nào đó thông qua sự kết hợp với một object (nghĩa là một phương thức của một class) hơn là định nghĩa một class mới.
Khởi tạo object mới bằng cách thay đổi cấu trúc: Rất nhiều ứng dụng xây dựng hệ thống từ nhiều phần và các phần con. Các phần con lại khởi tạo từ nhiều phần con khác (chia nhỏ bài toán). Prototype pattern cũng hỗ trợ điều này. Nghĩa là các phần đó có thể được khởi tạo từ việc copy một nguyên mẫu từ một “cấu trúc” khác. Miễn là các phần kết hợp đều thể hiện clone() và được sử dụng với cấu trúc khác nhau làm nguyên mẫu. Xem thêm về Object cloning trong java bạn sẽ thấy rõ điều này.

##Sử dụng Prototype khi nào?
Chúng ta có một object và cần phải tạo 1 ọbject mới khác dựa trên object bạn đầu mà không thể sử dụng toán tử new hay các hàm contructor để khởi tạo. Vì sao vậy? Lý do đơn giản là ở đây chúng ta ko hề được biết thông tin nội tại của object đó hoặc object đó đã có thể bị che dấu đi nhiều thông tin khác mà chỉ cho ta một thông tin rất giới hạn không đủ để hiểu được. Do vậy ta ko thể dùng toán tử new để khởi tạo nó được. Giải pháp: để cho chính object mẫu tự xác định thông tin và dữ liệu sao chép.
Khởi tạo đối tượng lúc run-time: chúng ta có thể xác định đối tượng cụ thể sẽ được khởi tạo lúc runtime nếu class được implement / extend từ một Prototype.
Cấu hình một ứng dụng với dynamic class.
Muốn truyền đối tượng vào một hàm nào đó để xử lý, thay vì truyền đối tượng gốc có thể ảnh hưởng dữ liệu thì ta có thể truyền đối tượng sao chép.
Chi phí của việc tạo mới đối tượng (bằng cách sử dụng toán tử new) là lớn.
Ẩn độ phức tạp của việc khởi tạo đối tượng từ phía Client.
```

```csharp
*Singleton
Despite noble intentions, the Singleton pattern described by the Gang of Four usually does more harm than good. They stress that the pattern should be used sparingly
=> Singleton Pattern nên được sử dụng 1 cách hợp lý (tránh sử dụng pattern này)

Theo định nghĩa của Gang of Four : "Ensure a class has one instance, and provide a global point of access to it."

-Nó hạn chế class chỉ có 1 instance duy nhất.
-Nó có thể truy cập ở bất kỳ đâu (global access)

Trước khi quyết định cho class đó là singleton bạn nên tự hỏi xem liệu có thể không cần dùng singleton mà vẫn giải quyết được vấn đề hay không?
```



### Object Pool Pattern

-Improve performance and memory use by reusing objects from a fixed pool instead of allocating and freeing them individually.

`Cải thiện hiệu suất và sử dụng bộ nhớ bằng cách sử dụng lại các đối tượng từ một nhóm cố định thay vì phân bổ và giải phóng chúng riêng lẻ.`

-Define a pool class that maintains a collection of reusable objects. Each object supports an “in use” query to tell if it is currently “alive”. When the pool is initialized, it creates the entire collection of objects up front (usually in a single contiguous allocation) and initializes them all to the “not in use” state.

`Xác định một lớp pool duy trì một bộ sưu tập các đối tượng có thể tái sử dụng. Mỗi đối tượng đều hỗ trợ một người dùng trong sử dụng truy vấn của người dùng để xác định xem hiện tại có còn sống không. Khi pool được khởi tạo, nó sẽ tạo ra toàn bộ bộ sưu tập các đối tượng ở phía trước (thường là trong một phân bổ liền kề duy nhất) và khởi tạo tất cả chúng cho các trạng thái không sử dụng trạng thái.`

-When you want a new object, ask the pool for one. It finds an available object, initializes it to “in use”, and returns it. When the object is no longer needed, it is set back to the “not in use” state. This way, objects can be freely created and destroyed without needing to allocate memory or other resources.

`Khi bạn muốn một đối tượng mới, yêu cầu nhóm cho một đối tượng. Nó tìm thấy một đối tượng có sẵn, khởi tạo nó để sử dụng, và trả về nó. Khi đối tượng không còn cần thiết nữa, nó được đặt trở lại trạng thái không sử dụng trạng thái. Bằng cách này, các đối tượng có thể được tạo và hủy tự do mà không cần phân bổ bộ nhớ hoặc các tài nguyên khác.`

### When to Use It

This pattern is used widely in games for obvious things like game entities and visual effects, but it is also used for less visible data structures such as currently playing sounds. Use Object Pool when:

- You need to frequently create and destroy objects.
- Objects are similar in size.
- Allocating objects on the heap is slow or could lead to memory fragmentation.
- Each object encapsulates a resource such as a database or network connection that is expensive to acquire and could be reused.

`Mẫu này được sử dụng rộng rãi trong các trò chơi cho những thứ rõ ràng như thực thể trò chơi và hiệu ứng hình ảnh, nhưng nó cũng được sử dụng cho các cấu trúc dữ liệu ít nhìn thấy hơn như âm thanh hiện đang phát. Sử dụng Object Pool khi`

- `Bạn cần thường xuyên tạo và phá hủy các đối tượng.`
- `Đối tượng có kích thước tương tự nhau.`
- `Phân bổ các đối tượng trên heap là chậm hoặc có thể dẫn đến phân mảnh bộ nhớ.`
- `Mỗi đối tượng đóng gói một tài nguyên như cơ sở dữ liệu hoặc kết nối mạng đắt tiền để có được và có thể được sử dụng lại.`


## License

Copyright (c) 2019 yenmoc

Released under the MIT license, see [LICENSE.txt](LICENSE.txt)

